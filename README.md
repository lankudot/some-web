# MLGameWeb

The web server for running the MLGame online version

## Set up

### Environment

* Docker 19.03.5
* docker-compose 1.23.2
* Python 3.6+ and install the following package:
    * channels_redis==2.4.0
    * docker

### PostgresDB

1. Copy or rename `compose-env/postgres.env.template` to `compose-env/postgres.env`
2. Edit the user and the password in the file for accessing the postrgresDB

### Nginx

1. Copy or rename `nginx/nginx.conf.template` to `nginx/nginx.conf`
2. Edit the setting in the file if needed:
    * `worker_connections`: The maximum number of connections (not only the connections to the client) that can be opened by a worker process
    * `client_max_body_size`: The maximum allowed size of the client request body
3. Copy or rename `nginx/web.conf.template` to `nginx/web.conf`
4. Replace the `www.example.com` in the file with the domain name or the IP of the server, or remove it but keep the semicolon.
5. Create directory `ssl/nginx/` at the project root directory
6. Create the self-signed certificate by the command:

```
$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ssl/nginx/nginx.key -out ssl/nginx/nginx.crt
```

### Django

1. Copy or rename `compose-env/web.env.template` to `compose-env/web.env`
2. Edit the value in the file if needed

### MLGame docker image

1. Create directory `game` at the project root directory
2. Put the MLGame online version to the directory
3. Build the MLGame docker image by the command:

```
$ sudo docker build -t mlgame -f game/Docker/Dockerfile game/
```

### MLGameWeb docker image

#### Choose the user for the service

The uid or gid of the default user in the container may be used in the host machine.
Setting the ownership of the mounted volume in the container will reflect to the host machine,
which sets the onwership to a different user having the same uid or gid in the host machine.
For example, the uid of user `nginx` in the container is 101, but it is `systemd-resolve` in the Ubuntu.
Therefore, you can use your id or create a system user for the service to avoid such condition.

If you want to create a system user for the service, use the command:

```
$ sudo useradd --system --user-group --shell=/usr/sbin/nologin mlgameweb
```

#### Build the docker image

1. Get your user id by the command:

```
$ id -u
```

or

```
$ id -u mlgameweb
```

for the system user.

2. Pass the user id you choose and build images:

```
$ sudo USER_ID=<id> docker-compose build
```

Always remember to pass the user id when building service images.
Note that do not use `sudo USER_ID=$(id -u) docker-compose build` to build the image.
Because `id -u` in sudo command will get 0 (root).

## Start the server

### The web server

#### Start the server

Start the web server by the command:

```
$ sudo docker-compose up
```

It is fine to ignore the message 'The USER\_ID variable is not set. Defaulting to a blank string.',
because this variable is only used for building images.

#### First time of starting the srever

For the first time of starting the server, there are several things to do. Open a new terminal.

1. Change the ownership of log directory in the `nginx_server` container:

```
$ sudo docker exec -it -u root nginx_server chown -R appuser:appuser /tmp/log/nginx/
```

2. Attach a shell to the `mlgameweb` container and the shell working directory in the container will be the `/app`:

```
$ sudo docker exec -it -u root mlgameweb bash
/app #
```

3. Do the database migration for Django by the command:

```
/app # python manage.py migrate
```

4. Create an admin user to manage Django. Run the command and follow hints:

```
/app # python manage.py createsuperuser
```

5. Collect static files in Django for Nginx by the command:

```
/app # python manage.py collectstatic
```

6. Change the ownership of `var/` for Django to save the downloaded player codes:

```
/app # chown -R appuser:appuser var/
```

7. And then quit the shell:

```
/app # exit
```

#### Stop the server

Press the Ctrl+C . To clear the created docker resource for the server by the command:

```
$ sudo docker-compose down
```

### Game container creator

Run the game container creator separately for creating the game container.

#### Config the container to be created

The container config is at the `ContainerConfig` section in the file `container_creator.ini`.
* `user_code_root`: The path of the root directory for storing the user code
* `docker_image`: The target image for the container
* `cpu_limit`: The number of cpu for the container
* `memory_limit`: The amount of the memory for the container
* `redis_network_name`: The docker network name where the redis server runs on
* `redis_host`: The host name of the redis server in the `redis_network`
* `redis_port`: The port of the redis server in the `redis_network`

#### Start the creator

Open a new terminal and run the command below. To create game container, the `sudo` permission is needed.

```
$ sudo python scripts/container_creator.py
```

**Options**

* `--no-rm`: Not to add `--rm` flag while creating the container
* `--no-create`: Not to create the container
* `--debug`: Output the detailed message

#### Stop the creator

Press the Ctrl+C
