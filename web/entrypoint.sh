#!/bin/bash

set -e

# Create directory for the program cache
USER_PROGRAM_BASE_DIR=/app/var/userProgram
USER_PROGRAM_CACHE_DIR=/app/var/userProgramCache
if [ ! -d "$USER_PROGRAM_BASE_DIR" ]; then
    mkdir -p "$USER_PROGRAM_BASE_DIR"
    chown appuser:appuser "$USER_PROGRAM_BASE_DIR"
fi
if [ ! -d "$USER_PROGRAM_CACHE_DIR" ]; then
    mkdir -p "$USER_PROGRAM_CACHE_DIR"
    chown appuser:appuser "$USER_PROGRAM_CACHE_DIR"
fi

# Start the cron service for clearing the program cache
service cron start

# Execute the CMD
exec gosu appuser "$@"
