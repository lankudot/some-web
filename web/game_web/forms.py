from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .tools.validator import FileValidator

# ===== File validators =====
source_code_validator = FileValidator(("py",),
    ("text/x-python", "text/plain"),
    max_size = 1 * 1024 * 1024)
model_file_validator = FileValidator(("pickle", "sav"),
    ("application/octet-stream",),
    max_size = 40 * 1024 * 1024)
zip_file_validator = FileValidator(("zip", ),
    ("application/zip",),
    max_size = 40 * 1024 * 1024)

FPS_CHOICES = (
    (24, "24"),
    (30, "30"),
    (45, "45"),
    (60, "60"),
    (75, "75"),
)
MODEL_FILE_CHOICES = (
    ("sav", _("單個模型檔")),
    ("zip", _("多個模型檔")),
)

def _add_game_param_fields(form, game_params):
    """
    Add additional fields for the game parameters

    The name of field is GameParameter.name,
    the label is the GameParameter.label_name,
    and the choices are generated from GameParameter.choices.

    The order of the added parameter fields is decided by GameParamenter.order.

    @param game_params A list of GameParameter instances
    @return A list of the name of the added field
    """
    added_field_name = []

    for param in game_params:
        choices = _generate_choices(param.get_choice_value_list())
        form.fields[param.name] = forms.ChoiceField(
            choices = choices, initial = choices[0][0],
            label = param.label_name)
        added_field_name.append(param.name)

    return added_field_name

def _generate_choices(choice_value_list):
    """
    Generate choices from GameParamter.choice for the ChoiceField.
    """
    field_choices = []

    for choice in choice_value_list:
        field_choices.append((choice, str(choice)))

    return field_choices

class PlayerCodeUploadForm(forms.Form):
    """
    The form for players to set game parameters and upload their code
    """

    fps = forms.ChoiceField(choices = FPS_CHOICES, initial = "24",
        label = _("執行的 FPS"))

    def __init__(self, num_of_players, game_params, *args, **kargs):
        """
        Constructor

        @param num_of_players Specify the number of players
        @param game_params Specify a list of GameParameter of the game
        """
        super().__init__(*args, **kargs)

        _add_game_param_fields(self, game_params)
        self._add_code_upload_field(num_of_players)

    def _add_code_upload_field(self, num_of_players):
        for i in range(num_of_players):
            player_tag = "-{}P".format(i + 1)
            self.fields["source_code_file_" + str(i)] = forms.FileField(
                max_length = 50,
                validators = [source_code_validator],
                label = _("機器學習程式碼") + player_tag,
                help_text = _("副檔名為 .py, "
                "檔案大小上限為 1 MB, 檔名不能超過 50 字元(包含副檔名)"))

            self.fields["model_file_choice_" + str(i)] = forms.ChoiceField(
                choices = MODEL_FILE_CHOICES,
                initial = MODEL_FILE_CHOICES[0][0],
                label = _("選擇上傳的模型檔類型"))
            self.fields["model_file_" + str(i)] = forms.FileField(
                required = False,
                max_length = 50,
                validators = [model_file_validator],
                label = _("模型檔 (選填)") + player_tag,
                help_text = _("附檔名為 .sav 或 .pickle, "
                "檔案大小上限為 40 MB, 檔名不能超過 50 字元(包含副檔名)"))
            self.fields["zip_file_" + str(i)] = forms.FileField(
                required = False,
                max_length = 50,
                validators = [zip_file_validator],
                label = _("壓縮檔 (選填)") + player_tag,
                help_text = _("附檔名為 .zip, "
                "檔案大小上限為 40 MB, 檔名不能超過 50 字元(包含副檔名)"))

class PlayerCodeURLForm(forms.Form):
    """
    The form for players to specify URL of the source code
    """
    def __init__(self, num_of_players, is_dynamic_player_num,
        game_params: dict, *args, **kargs):
        super().__init__(*args, **kargs)

        _add_game_param_fields(self, game_params)
        self._add_url_field(num_of_players, is_dynamic_player_num)

    def _add_url_field(self, num_of_players, is_dynamic_player_num):
        for i in range(num_of_players):
            field_required = True if (i == 0 or not is_dynamic_player_num) else False

            player_tag = "-{}P".format(i + 1)
            self.fields["repo_name_" + str(i)] = forms.CharField(
                required = field_required,
                label = _("程式碼 repo 名稱") + player_tag,
                help_text = _("指定程式碼存放 repo 的名稱"))
            self.fields["tag_" + str(i)] = forms.CharField(
                required = field_required,
                max_length = 25,
                label = _("版本 tag") + player_tag,
                help_text = _("指定要使用的版本"))
