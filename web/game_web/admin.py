from django.contrib import admin
from django.db import models
from django.forms import widgets
from game_web.models import Game, Room, GameParameter, UserProgramCache

@admin.register(GameParameter)
class GameParameterAdmin(admin.ModelAdmin):
    list_display = ("name", "label_name", "game", "order", "choices")
    list_filter = ("game",)

class GameParameterInline(admin.TabularInline):
    model = GameParameter
    ordering = ["order"]
    formfield_overrides = {
        models.TextField: {
            "widget": widgets.Textarea(attrs = {"rows": "1", "cols": "40"})
        }
    }
    extra = 1   # Set extra entry to 1

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ("name", "id", "player_num", "is_dynamic_player_num")
    list_filter = ("player_num",)

    inlines = [GameParameterInline]

@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ("game", "short_room_id", "created_time")
    list_filter = ("game",)

    readonly_fields = ("id", "game", "created_time")

@admin.register(UserProgramCache)
class UserProgramCacheAdmin(admin.ModelAdmin):
    list_display = ("short_hash_string", "repo_name",
        "repo_tag_or_branch", "last_used_time")
    list_filter = ("last_used_time", )

    readonly_fields = ("hash_value", "repo_name",
        "repo_tag_or_branch", "last_used_time")
