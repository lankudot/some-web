from django.apps import AppConfig


class GameWebConfig(AppConfig):
    name = 'game_web'
