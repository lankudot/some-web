"""
The exceptions for the application game_web
"""

class GameCommandError(Exception):
    """
    The exception raised when there has invalid argument in the `GameCommand`
    """
    pass

class UserProgramInfoError(Exception):
    """
    The exception raised while handling the user program information
    """
    pass
