from enum import Enum

class INFO_ID(Enum):
    DOWNLOADING_USER_CODE = 10
    CREATING_GAME = 11
    GAME_CREATED = 12

    def __str__(self):
        return self.value

class ERROR_ID(Enum):
    JSON_FORMAT_ERROR = 1
    LACK_CMD_PARAMETER = 2
    UNKNOWN_CMD = 3
    INVALID_GAME_CMD = 10
    UNABLE_TO_DOWNLOAD_USER_CODE = 11
    UNABLE_TO_CREATE_GAME = 12
    GAME_PROGRAM_ERROR = 13
    TOO_MANY_RUNNING_GAMES = 14
    UNHANDLED_ERROR = 99

    def __str__(self):
        return self.value
