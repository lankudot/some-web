from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from asgiref.sync import sync_to_async
import json
import logging
import sys

from .exceptions import GameCommandError, UserProgramInfoError
from .models import Room, Game
from .messages import INFO_ID, ERROR_ID
from .tools import userprogram_manager, gamecommand

logger = logging.getLogger(__name__)

class GameProgressConsumer(AsyncWebsocketConsumer):
    """
    A websocket consumer for delivering the game progress to the front end

    @var _game_cmd A GameCommand object storing the received game command
    @var _room_id The unique ID of the game room
    @var _is_game_started Whether the corresponding game container is started or not
    """

    async def connect(self):
        """
        Accept the websocket connection and initialize attributes

        This function is invoked when a new websocket connection is established.
        """
        await self.accept()

        self._game_cmd = None
        self._room_id = None
        self._short_room_id = None
        self._is_game_started = False

    async def disconnect(self, close_code):
        """
        Remove the room created and inform the container creator to stop the running game

        This function is invoked when the websocket is disconnected.

        @param close_code The code of the reason of the disconnection
        """
        if self._room_id:
            await self._delete_room_entry(self._room_id)
            sync_to_async(userprogram_manager.delete_room_dir(self._room_id))

        # If the game is still running,
        # inform the creator to stop the game container.
        if self._is_game_started:
            await self.channel_layer.send("container_creator", {
                "action": "stop",
                "room_id": self._room_id,
            })

        self._logger_info("Room closed")

    async def receive(self, text_data = None, byte_data = None):
        """
        Receive the command sent from the front end

        This function is invoked when the websocket connection receives the message.
        """
        try:
            text_data_dict = json.loads(text_data)
        except json.JSONDecodeError as e:
            # The received data is not a valid JSON format
            await self._send_error_and_close(ERROR_ID.JSON_FORMAT_ERROR, str(e))
            self._logger_error(str(e))
            self._logger_error("Received data: {}".format(text_data))
        else:
            await self._cmd_handler(text_data_dict)

    async def send(self, type: str, data: dict):
        """
        Send the data to the front end

        @param type The type of the data
        @param data A dictionary for storing the data
        """
        await super().send(text_data = json.dumps({
            "type": type,
            "data": data
        }))

    async def _system_message(self, level, msg_id, message):
        """
        Send the system message to the front end

        @param level The message level
        @param msg_id The id of the type of message. It could be `messages.INFO_ID`
               or `messages.ERROR_ID`.
        @param message The detailed message
        """
        await self.send("system_message", {
            "level": level,
            "id": msg_id.value,
            "message": message,
        })

    async def _send_info(self, msg_id, message):
        """
        Send the info message to the front
        """
        await self._system_message("info", msg_id, message)

    async def _send_error_and_close(self, msg_id, message):
        """
        Send the error message to the front end and close the websocket
        """
        await self._system_message("error", msg_id, message)
        await self.close()

    async def _cmd_handler(self, cmd_dict):
        """
        Handle the command received

        It will invoke the corresponding handler according to the "type" of the command.

        @param cmd_dict The dictionary storing the context of the received command
        """
        try:
            if cmd_dict["type"] == "game_cmd":
                handler = self._game_cmd_handler
                data = cmd_dict["data"]
            else:
                raise ValueError
        except KeyError as e:   # Missing 'type' or 'data' in the `cmd_dict`
            msg = "Missing {} field in received command".format(e)
            self._logger_error(msg)
            await self._send_error_and_close(ERROR_ID.LACK_CMD_PARAMETER, msg)
        except ValueError:
            msg = "Received unknown command: '{}'".format(cmd_dict["type"])
            self._logger_error(msg)
            await self._send_error_and_close(ERROR_ID.UNKNOWN_CMD, msg)
        else:
            await handler(data)

    async def _game_cmd_handler(self, data_dict):
        """
        Handle the game creation command

        @param cmd_data A dictionary containing the data in the game command
        """
        # Validate the game creation command
        try:
            self._game_cmd = (
                await database_sync_to_async(gamecommand.generate_game_command)(data_dict))
        except GameCommandError as e:
            await self._send_error_and_close(ERROR_ID.INVALID_GAME_CMD, str(e))
            self._logger_error("Invalid game command: " + str(e))
            return
        except Exception:
            msg = "Unexpected error occurred whlie parsing game command"
            await self._send_error_and_close(ERROR_ID.UNHANDLED_ERROR, msg)
            self._logger_critical(msg, sys.exc_info())
            return

        # Create room
        self._room_id = await self._create_room_entry(self._game_cmd.game_played)
        self._short_room_id = self._room_id[:8]

        # Try to download the user code
        msg = "Downloading the user code"
        await self._send_info(INFO_ID.DOWNLOADING_USER_CODE, msg)
        self._logger_info(msg)

        try:
            await (database_sync_to_async(userprogram_manager.create_room_dir)
                (self._room_id, self._game_cmd.program_ids))
        except UserProgramInfoError as e:
            await self._send_error_and_close(ERROR_ID.UNABLE_TO_DOWNLOAD_USER_CODE,
                str(e))
            return
        except Exception:
            msg = "Unexpected error occurred whlie downloading user code"
            await self._send_error_and_close(ERROR_ID.UNHANDLED_ERROR, msg)
            self._logger_critical(msg, sys.exc_info())
            return

        # Inform the container creator to start the game
        await self.channel_layer.send("container_creator", {
            "action": "create",
            "channel_name": self.channel_name,
            "room_id": self._room_id,
            "game_cmd": {
                "game_name": self._game_cmd.game_played.name,
                "game_param": self._game_cmd.game_param_values,
                "num_of_players": len(self._game_cmd.program_ids),
            },
        })

        self._is_game_started = True
        msg = "Creating the game"
        await self._send_info(INFO_ID.CREATING_GAME, msg)
        self._logger_info(msg)

    ### The callback functions for the container_creator ###

    async def creator_message(self, event):
        """
        Handle the message sent from the container_creator
        """
        returncode = event["returncode"]

        if returncode == 0:
            msg = "Game created"
            await self._send_info(INFO_ID.GAME_CREATED, msg)
            self._logger_info(msg)
        elif returncode == 1:
            self._is_game_started = False
            msg = "Unable to create game"
            await self._send_error_and_close(ERROR_ID.UNABLE_TO_CREATE_GAME, msg)
            self._logger_error(msg)
        elif returncode == 2:
            self._is_game_started = False
            msg = "Too many running games"
            await self._send_error_and_close(ERROR_ID.TOO_MANY_RUNNING_GAMES, msg)
            self._logger_error("Unable to create game. " + msg)

    ### The callback functions for the game container to pass data to the front end ###

    async def game_info(self, event):
        """
        Pass the game information to the front end
        """
        # Insert the game ID to the data
        event["data"]["game_id"] = self._game_cmd.game_id
        await self.send("game_info", event["data"])

    async def game_progress(self, event):
        """
        Pass the game progress to the front end
        """
        await self.send("game_progress", event["data"])

    async def game_error(self, event):
        """
        Pass the game error to the front end and close the websocket
        """
        msg = event["data"]["message"]
        await self._send_error_and_close(ERROR_ID.GAME_PROGRAM_ERROR, msg)
        self._logger_error(msg.splitlines()[0])
        self._is_game_started = False

    async def game_result(self, event):
        """
        Pass the game result to the front end
        """
        await self.send("game_result", event["data"])
        self._is_game_started = False
        await self.close()

    ### The functions for accessing the database ###

    @database_sync_to_async
    def _create_room_entry(self, game_played) -> str:
        """
        Create a new Room and save to the database

        @return The ID of the created room
        """
        room = Room(game = game_played)
        room.save()

        return str(room.id)

    @database_sync_to_async
    def _delete_room_entry(self, room_id):
        """
        Delete the specified game room from the database
        """
        try:
            room = Room.objects.get(id = room_id)
            room.delete()
        except Room.DoesNotExist:
            pass

    ### The functions for the logger ###

    def _logger_info(self, message):
        logger.info(self._get_log_prefix() + message)

    def _logger_error(self, message):
        logger.error(self._get_log_prefix() + message)

    def _logger_critical(self, message, exc_info):
        logger.critical(self._get_log_prefix() + message, exc_info = exc_info)

    def _get_log_prefix(self):
        if self._short_room_id:
            return "[consumer] Room '{}': ".format(self._short_room_id)
        else:
            return "[consumer] "
