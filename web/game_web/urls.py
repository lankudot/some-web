from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name = "index"),
    path("<int:pk>/upload/", views.player_code_upload, name = "player-code-upload"),
    path("<int:pk>/room/", views.game_room, name = "game-room"),
    path("api/codeToken/<int:program_id>", views.get_code_token, name = "get-code-token"),
]
