from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import MinValueValidator
import uuid

class Game(models.Model):
    name = models.CharField(
        max_length = 15,
        verbose_name = _("遊戲名稱"))
    player_num = models.PositiveSmallIntegerField(
        default = 1,
        verbose_name = _("玩家人數"),
        validators = [MinValueValidator(1)])
    is_dynamic_player_num = models.BooleanField(
        default = False,
        verbose_name = _("變動玩家人數"))

    class Meta:
        ordering = ["id"]
        verbose_name = _("遊戲")
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    def get_player_code_upload_url(self):
        return reverse("player-code-upload", args=[str(self.id)])

    def get_game_params(self):
        """
        Generate a list of ordered game params

        If the Game has no game params, return an empty list.
        """
        try:
            params = self.game_params.order_by("order")
            if len(params) == 0:
                return []

        except GameParameter.DoesNotExist:
            return []
        else:
            return list(params)

class GameParameter(models.Model):
    """
    Additional parameters for the game
    """
    game = models.ForeignKey(Game,
        on_delete = models.CASCADE,
        related_name = "game_params",
        verbose_name = _("所屬遊戲"))

    name = models.CharField(
        max_length = 15,
        verbose_name = _("參數名稱"))
    label_name = models.CharField(
        max_length = 30,
        verbose_name = _("參數顯示名稱"))

    order = models.IntegerField(
        default = 1,
        verbose_name = _("參數順序"),
        help_text = _("一個遊戲中的參數順序值不能重複"))
    choices = models.TextField(
        verbose_name = _("可選選項"),
        help_text = _("選項間用 ',' 隔開，"
        "用 '-' 可以指定連續選項 (僅限數字)"))

    class Meta:
        ordering = ["game", "order"]
        verbose_name = _("遊戲參數")
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    def get_choice_value_list(self):
        """
        Get a list of choice values generated from `choices`.

        For example, "1-3, 5" will be ["1", "2", "3", "5"].
        """
        str_list = self.choices.replace(" ", "").split(",")
        value_list = []

        for choice in str_list:
            # Range of numbers
            if "-" in choice:
                choice_range = choice.split("-")
                for i in range(int(choice_range[0]), int(choice_range[1]) + 1):
                    value_list.append(str(i))
                continue

            value_list.append(str(choice))

        return value_list

class Room(models.Model):
    """
    Game room for executing the game.
    """
    id = models.UUIDField(
        primary_key = True,
        default = uuid.uuid4,
        editable = False,
        verbose_name = _("房間 ID"))
    game = models.ForeignKey(Game,
        on_delete = models.CASCADE,
        editable = False,
        verbose_name = _("進行的遊戲"))
    created_time = models.DateTimeField(
        auto_now = True,
        editable = False,
        verbose_name = _("建立的時間"))

    class Meta:
        ordering = ["created_time", "game", "id"]
        verbose_name = _("遊戲房間")
        verbose_name_plural = verbose_name

    def __str__(self):
        return "{} ({})".format(self.game.name, self.id)

    def short_room_id(self):
        """
        Get the first section of room id
        """
        return self.id.__str__().split("-")[0]

    short_room_id.short_description = _("簡短房間 ID")

class UserProgramCache(models.Model):
    """
    Record the downloaded user program
    """
    hash_value = models.CharField(
        primary_key = True,
        editable = False,
        max_length = 64,
        verbose_name = _("雜湊值"))
    repo_name = models.CharField(
        editable = False,
        max_length = 200,
        verbose_name = _("repo 名稱"))
    repo_tag_or_branch = models.CharField(
        editable = False,
        max_length = 100,
        verbose_name = _("repo 標籤或分支"))
    last_commit = models.CharField(
        max_length = 64,
        verbose_name = _("commit sha 值"))
    last_used_time = models.DateTimeField(
        auto_now = True,
        verbose_name = _("上次使用時間"))

    class Meta:
        ordering = ["-last_used_time"]
        verbose_name = _("暫存使用者程式")
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.hash_value

    def get_absolute_url(self):
        return self.__str__()

    def short_hash_string(self):
        """
        Get only first 16 characters of hex representation of hash_value
        """
        return self.hash_value[:8] + "-" + self.hash_value[8:16]

    short_hash_string.short_description = _("簡短雜湊值")

    def short_last_commit_string(self):
        """
        Get only first 8 characters of `last_commit`
        """
        return self.last_commit[:8]

    short_last_commit_string.short_description = _("簡短 commit 值")
