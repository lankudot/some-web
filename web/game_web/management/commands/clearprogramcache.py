from django.core.management.base import BaseCommand, CommandError
from datetime import datetime, timedelta
from pytz import timezone

from MLGameWeb.settings import TIME_ZONE
from game_web.tools.userprogram_manager import clear_user_program_cache

class Command(BaseCommand):
    help = "Clear the expired user program caches"

    def add_arguments(self, parser):
        parser.add_argument("--days", "-d",
            dest = "days_before",
            type = non_negative_int,
            default = 7,
            help = "Specify the days before now. The program caches that "
                   " aren't used during this period will be cleared. "
                   "[default: $(default)s]")

    def handle(self, *args, **options):
        expired_datetime = (datetime.now(tz = timezone(TIME_ZONE)) -
            timedelta(days = options["days_before"]))
        total_expired_caches, cleared_caches = clear_user_program_cache(expired_datetime)
        print("{} expired caches found, {} deleted.".format(total_expired_caches, cleared_caches))

def non_negative_int(string):
    value = int(string)
    if not value >= 0:
        raise ValueError("The value must be a non-negative integer.")
    return value