from django.shortcuts import render, get_object_or_404
from django.http import (
    HttpResponse, HttpResponseRedirect, FileResponse, HttpResponseNotFound
)
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from .models import Game, Room
from .forms import PlayerCodeURLForm
from .tools import userprogram_manager
from .settings import GITHUB_USER_NAME, GITHUB_USER_TOKEN, VARIABLE_DATA_DIR

import json
import os.path
import pickle

def index(request):
    """
    Display the available games for player to choose
    """
    available_games = Game.objects.all()
    num_of_rooms = Room.objects.all().count()

    context = {
        "available_games": available_games,
        "num_of_rooms": num_of_rooms,
    }

    return render(request, "index.html", context = context)

@login_required
def player_code_upload(request, pk):
    """
    Upload the machine learning code for the game selected
    and then redirect to the game room

    @param pk The primary key of the Game
    """
    game = get_object_or_404(Game, pk = pk)
    num_of_players = game.player_num
    is_dynamic_player_num = game.is_dynamic_player_num

    game_params = game.get_game_params()

    if request.method == "POST":
        form = PlayerCodeURLForm(num_of_players, is_dynamic_player_num, game_params,
            request.POST, request.FILES)

        if form.is_valid():
            # Parse the repo name and tag
            programs = []
            for i in range(num_of_players):
                # If there has empty field, break it.
                if not form.cleaned_data["repo_name_" + str(i)]:
                    break

                programs.append({"id": i})
                # Write the repo information to the file for `get_code_token`
                with open(os.path.join(
                    VARIABLE_DATA_DIR, "repo_data_{}.pickle").format(i), "wb") as p:
                    pickle.dump({
                        "repo_name": form.cleaned_data["repo_name_" + str(i)],
                        "tag": form.cleaned_data["tag_" + str(i)]
                    }, p)

            # Parse the game params
            received_game_params = {}
            for param in game_params:
                received_game_params[param.name] = form.cleaned_data[param.name]

            # Store the game options and params to the session data
            request.session["game_cmd"] = {
                "game_id": pk,
                "game_param": received_game_params,
                "programs": programs
            }
            return HttpResponseRedirect(reverse("game-room", args = [str(pk)]))
    else:
        form = PlayerCodeURLForm(num_of_players, is_dynamic_player_num, game_params)

    context = {
        "form": form,
        "game": game,
        "num_of_players": num_of_players,
    }

    return render(request, "game_web/player_code_upload.html",
        context = context)

@login_required
def game_room(request, pk):
    """
    Display the game progress

    The rendered web page will establish a websocket for receiving the game progress.
    The created websocket is served by the consumers.GameProgressConsumer

    @param pk The primary key of the Game
    """
    game = get_object_or_404(Game, pk = pk)

    game_cmd = {
        "type": "game_cmd",
        "data": request.session["game_cmd"]
    }

    context = {
        "game": game,
        "game_cmd": game_cmd.__str__().replace("\'", "\\\""),
    }

    return render(request, "game_web/game_room.html", context = context)

def get_code_token(request, program_id):
    """
    Return the predefined code token for the testing purpose

    This method simulates the response returning from the frontend for querying the code token
    """
    # Get the repo data from the saved file
    file_path = os.path.join(VARIABLE_DATA_DIR, "repo_data_{}.pickle".format(program_id))
    with open(file_path, "rb") as p:
        repo_data = pickle.load(p)
    os.remove(file_path)

    repo_data["account"] = GITHUB_USER_NAME
    repo_data["token"] = GITHUB_USER_TOKEN
    repo_data["id"] = program_id

    return HttpResponse(json.dumps(repo_data))
