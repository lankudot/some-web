"""
The settings for the game_web application
"""

import os.path

from MLGameWeb.settings import BASE_DIR

VARIABLE_DATA_DIR = os.path.join(BASE_DIR, "var")

# The directories for storing the user program

USER_PROGRAM_BASE_DIR = os.path.join(VARIABLE_DATA_DIR, "userProgram")
USER_PROGRAM_CACHE_DIR = os.path.join(VARIABLE_DATA_DIR, "userProgramCache")

# Github user token

# The url for querying user token
# The url "http://localhost:8000/game/api/codeToken/" is used for testing purpose,
# which will return the predefined user token.
QUERY_USER_TOKEN_URL = os.environ.get("QUERY_USER_TOKEN_URL",
    "http://localhost:8000/game/api/codeToken/")

# If the QUERY_USER_TOKEN_URL is "http://localhost:8000/game/api/codeToken/",
# this value will be returned.
GITHUB_USER_NAME = os.environ.get("GITHUB_USER_NAME", "")
GITHUB_USER_TOKEN = os.environ.get("GITHUB_USER_TOKEN", 0)
