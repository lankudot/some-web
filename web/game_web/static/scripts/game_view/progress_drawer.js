class GameProgressDrawer
{
    /**
     * Initialize the size of canvas and the draw content.
     * 
     * @param canvas The HTML canvas element
     * @param gameInfo The JSON object that stores the game information
     */
    constructor(canvas, gameInfo)
    {
        this.scene = {
            width: gameInfo["scene"]["size"][0],
            height: gameInfo["scene"]["size"][1]
        };

        canvas.width = this.scene.width;
        canvas.height = this.scene.height;
        this.ctx = canvas.getContext("2d");

        this.gameObjectRect = {};
        let gameObjectArray = gameInfo["game_object"];
        for (let object of gameObjectArray) {
            this.gameObjectRect[object["name"]] = {
                width: object["size"][0],
                height: object["size"][1],
                color: "rgb(" + object["color"][0] + ", " + object["color"][1] + ", " + object["color"][2] + ")"
            };
        }
    }

    /**
     * Draw the scene from the JSON object
     * which stores the information of the game scene.
     * 
     * @param gameObjectInfo The JSON object that stores the scene information
     */
    drawFromJSON(gameObjectInfo)
    {
        this.ctx.fillStyle = "Black";
        this.ctx.fillRect(0, 0, this.scene.width, this.scene.height);

        let gameObjectNames = Object.keys(gameObjectInfo);
        for (let objectName of gameObjectNames) {
            this.ctx.fillStyle = this.gameObjectRect[objectName].color;

            let posArray = gameObjectInfo[objectName];
            for (let pos of posArray) {
                this.ctx.fillRect(pos[0], pos[1],
                    this.gameObjectRect[objectName].width,
                    this.gameObjectRect[objectName].height);
                this.ctx.strokeRect(pos[0], pos[1],
                    this.gameObjectRect[objectName].width,
                    this.gameObjectRect[objectName].height);
            }
        }
    }
}