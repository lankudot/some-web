from ..models import Game
from ..exceptions import GameCommandError

class GameCommand:
    """
    The data class for storing the game command

    @var game_id The ID of the game
    @var game_played The `Game` object of the game to be played
    @var game_param_dict A dictionary containing received game parameters.
         The key is the name of the parameter, the value is the value specified for that
         parameter.
    @var game_param_values A list containing the game parameter values parsed from
         `game_param_dict`, and their order is sorted according to the `GameParameter.order`.
    @var program_ids A list storing the id of user programs
    """

    def __init__(self, data_dict):
        """Constructor

        @param data_dict A dictionary containing the received game command data
        """
        self._parse_data_dict(data_dict)

    def _parse_data_dict(self, data_dict):
        """
        Parse the information from the `data_dict` and store the parsed value
        If there has data unmatched, `GameCommandError` exception will be raised.
        """
        # Check if the needed data is provided #
        try:
            self.game_id = data_dict["game_id"]
            self.game_param_dict = data_dict["game_param"]
            self.program_ids = []
            for program in data_dict["programs"]:
                self.program_ids.append(str(program["id"]))
        except KeyError as e:
            raise GameCommandError("{} is not specified.".format(e))

        # Check if the data is valid #
        try:
            self.game_played = self._get_game_entry()
            self.game_param_values = self._parse_game_param_values()

            num_of_players = self.game_played.player_num
            num_of_programs = len(self.program_ids)
            if (not self.game_played.is_dynamic_player_num and
                num_of_programs != num_of_players):
                raise GameCommandError("The number of programs is not matched. "
                    "{} is needed, but gets {}."
                    .format(num_of_players, num_of_programs))
            elif (self.game_played.is_dynamic_player_num and
                  num_of_programs > num_of_players):
                raise GameCommandError("Too many players. The max is {}, but gets {}"
                    .format(num_of_players, num_of_programs))
        except GameCommandError:
            raise

    def _get_game_entry(self) -> Game:
        """
        Get the `Game` entry from database according to the `game_id`
        """
        try:
            game = Game.objects.get(id = self.game_id)
        except Game.DoesNotExist:
            raise GameCommandError("Invalid game_id: {}".format(self.game_id))
        else:
            return game

    def _parse_game_param_values(self):
        """
        Parse and verify game parameter values specified in `game_param_dict`

        The order of parsed values in the list is decided by the order of
        the `GameParameter` of that game.
        """
        parsed_value_list = []

        try:
            game_param_list = self.game_played.get_game_params()

            for game_param in game_param_list:
                specified_value = str(self.game_param_dict[game_param.name])
                if specified_value in game_param.get_choice_value_list():
                    parsed_value_list.append(specified_value)
                else:
                    raise ValueError(game_param.name)

        except KeyError as e:
            raise GameCommandError("game_param {} is not specified".format(e))
        except ValueError as e:
            raise GameCommandError("Invalid value of game_param '{}'".format(e))
        else:
            return parsed_value_list

    def __str__(self):
        s = ("{\n" +
             "    'game_id': {},\n".format(self.game_id) +
             "    'game_name': '{}',\n".format(self.game_played.name) +
             "    'game_param_dict': {},\n".format(self.game_param_dict) +
             "    'game_param_values': {},\n".format(self.game_param_values) +
             "    'program_ids': {}\n".format(self.program_ids) +
             "}")

        return s

# Wrap the creation of the `GameCommand` as a function
# for the `GameComsumer` to invoke it from the asynchronous code
# with `database_sync_to_async`.
def generate_game_command(data_dict):
    return GameCommand(data_dict)