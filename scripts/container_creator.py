from argparse import ArgumentParser
from channels_redis.core import RedisChannelLayer
from configparser import ConfigParser
import asyncio
import docker
import logging
import traceback
import sys
import os.path

args = None
config = None
logger = None
USER_CODE_ROOT = ""

def parse_cmd_args():
    parser = ArgumentParser(
        description = "Listen to the redis server, and create the game container.")
    parser.add_argument("--no-rm", action = "store_true", dest = "no_rm_flag",
        help = "Not to add '--rm' flag while creating the container")
    parser.add_argument("--no-create", action = "store_true", dest = "not_create_container",
        help = "Not to create the container")
    parser.add_argument("--debug", action = "store_true",
        help = "Output the detailed debugging message. Such as the command for creating "
               "the container.")

    global args
    args = parser.parse_args()

def load_config():
    script_dir = os.path.dirname(__file__)

    global config
    config = ConfigParser()
    config.read(os.path.join(script_dir, "container_creator.ini"))

    global USER_CODE_ROOT
    user_code_dir = os.path.join(script_dir, config["ContainerConfig"]["user_code_root"])
    USER_CODE_ROOT = os.path.abspath(user_code_dir)

def init_logger():
    logging_level = logging.INFO if not args.debug else logging.DEBUG

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging_level)
    console_log_formatter = logging.Formatter(
            fmt = "[%(asctime)s] %(levelname)-8s %(message)s",
            datefmt = "%m-%d %H:%M:%S")
    console_handler.setFormatter(console_log_formatter)

    global logger
    logger = logging.getLogger("container_creator")
    logger.setLevel(logging_level)
    logger.addHandler(console_handler)

def listen_on_redis_server(server_ip, server_port, channel_name):
    """
    Wait for the information of container to be created sent from the redis server

    @param server_ip Specify the IP of the redis server
    @param server_port Specify the port of the redis server
    @param channel_name Specify the name of the channel to listen on the information
    """
    redis_server = RedisChannelLayer(hosts = [(server_ip, server_port)])
    logger.info("Listen on redis server {}:{} in channel \"{}\""
            .format(server_ip, server_port, channel_name))

    docker_client = docker.from_env()

    loop = asyncio.get_event_loop()

    while True:
        receive_msg_coroutine = redis_server.receive(channel_name)
        container_info = loop.run_until_complete(receive_msg_coroutine)

        action = container_info["action"]
        if action == "create":
            return_code = create_container(docker_client.containers, container_info)
            message = {"type": "creator_message", "returncode": return_code}
            send_msg_coroutine = redis_server.send(container_info["channel_name"], message)
            loop.run_until_complete(send_msg_coroutine)
        elif action == "stop":
            stop_container(docker_client.containers, container_info)

def create_container(container_client, container_info) -> int:
    """
    Create a docker container to run the game

    @param container_client The client for managing containers
    @param container_info The information for creating the game container
    @return value 0 if the container is created successfully
    """
    # Check if the running game containers is too much
    num_of_container = len(container_client.list(filters = {"name": "mlgame_"}))
    if num_of_container >= 20:
        logger.error("Failed to create container. "
            "Reason: There are too much running game containers.")
        return 2

    # Container information
    target_image = config["ContainerConfig"]["docker_image"]
    cpu_limit = config["ContainerConfig"]["cpu_limit"]
    memory_limit = config["ContainerConfig"]["memory_limit"]
    redis_network = config["ContainerConfig"]["redis_network_name"]

    # Game information
    room_id = container_info["room_id"]
    container_name = "mlgame_" + room_id.split('-')[0]
    game_cmd = container_info["game_cmd"]
    game_name = game_cmd["game_name"]

    # Transition channel information
    game_channel_name = container_info["channel_name"]
    online_channel_info = ("{}:{}:{}"
        .format(config["ContainerConfig"]["redis_host"],
                config["ContainerConfig"]["redis_port"],
                game_channel_name))

    # User code information
    ml_code_dir = os.path.join(USER_CODE_ROOT, room_id, "ml")

    # Generate the command
    ## Game command
    mlgame_cmd = [
        "python", "MLGame.py",
        "--transition-channel=" + online_channel_info,
        "-f", "30", "-1"
    ]
    ### Append the target player modules
    for i in range(game_cmd["num_of_players"]):
        mlgame_cmd.append("--input-module")
        mlgame_cmd.append("games.{}.ml.ml_{}.ml_play".format(game_name, i))
    ### Append the game parameters to the command
    mlgame_cmd.append(game_name)
    for param in game_cmd["game_param"]:
        mlgame_cmd.append(str(param))

    logger.debug("Redis target channel: {}, room id: {}, container name: {}"
        .format(game_channel_name, room_id, container_name))
    logger.debug("mlgame command: {}".format(" ".join(mlgame_cmd)))

    if args.not_create_container:
        return 0

    # Create container
    logger.info("Creating container {}".format(container_name))
    try:
        container_client.run(target_image, mlgame_cmd,
            name = container_name,
            detach = True,
            auto_remove = not args.no_rm_flag,
            mem_limit = memory_limit,
            nano_cpus = int(cpu_limit) * (10 ** 9),
            network = redis_network,
            volumes = {
                ml_code_dir: {
                    "bind": "/app/games/{}/ml" .format(game_name),
                    "mode": "rw"
                }
            },
        )
    except docker.errors.APIError as e:
        logger.error("Failed to create container {}. Reason: {}"
            .format(container_name, e))
        return 1
    else:
        logger.info("Container {} created".format(container_name))
        return 0

def stop_container(container_client, container_info):
    """
    Stop the running game container

    @param container_client The client for managing containers
    @param container_info The information of the game container
    @return 0 if the target game container is stopped successfully
    """
    container_name = "mlgame_" + container_info["room_id"].split('-')[0]

    logger.info("Stopping container {}".format(container_name))

    try:
        container = container_client.get(container_name)
        container.stop()
    except docker.errors.NotFound:
        logger.warning("Container {} not found".format(container_name))
        return 2
    except docker.errors.APIError as e:
        logger.error("Failed to stop container {}. Reason: {}"
            .format(container_name, e))
        return 1
    else:
        logger.info("Container {} stopped".format(container_name))
        return 0

if __name__ == "__main__":
    parse_cmd_args()
    load_config()
    init_logger()
    try:
        server_ip = config["ListenFrom"]["IP"]
        server_port = int(config["ListenFrom"]["Port"])
        channel_name = config["ListenFrom"]["channel_name"]

        logger.info("Starting the container creator. Stop it by Ctrl+C")
        logger.info("The PID is {}".format(os.getpid()))
        logger.info("USER_CODE_ROOT is at {}".format(USER_CODE_ROOT))
        listen_on_redis_server(server_ip, server_port, channel_name)
    except KeyboardInterrupt:
        logger.info("Stop the container creator")
    except Exception:
        logger.critical("Exception occured\n" + traceback.format_exc())
