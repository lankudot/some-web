# MLGameWeb Service Monitoring

## Configure Grafana

### Set Up Admin

1. Rename or copy `compose-env/grafana.env.template` to `compose-env/grafana.env`
2. Set the value of `GF_SECURITY_ADMIN_USER` and `GF_SECURITY_ADMIN_PASSWORD`

### Set Up Server URL

1. Rename or copy `grafana/config.ini.template` to `grafana/config.ini`
2. Set the value of `domain` and `root_url` to the hostname of the running server
3. Open the file `nginx/web.conf` and change the last url in the `server_name` in both two `server` block to the hostname of the running server

## Build

1. Check if the name of the nginx docker image built for the `mlgameweb` is `mlgameweb_nginx`. If not, modify the name of the image in `nginx/Dockerfile` to the correct name.
2. Run command `sudo docker-compose build` to build images

## Run

1. Make sure the service of `mlgameweb` is running
2. Run command `sudo docker-compose up` to start the monitoring
3. The dashboard page is avaliable at `<hostname>:3000` and login with username and password defined in `compose-env/grafana.env`
